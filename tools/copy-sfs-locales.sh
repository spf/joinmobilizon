for f in $(find ./node_modules/vue-fs-commons/app/locales -name 'sfs.yml' -type f); do
  lgdir=$(echo $f | cut -d '/' -f1,4-6);
  if [ -d $lgdir ]; then
    echo $lgdir/sfs.yml ✓;
    cp $f $lgdir/sfs.yml;
  else
    echo $lgdir/sfs.yml ✗;
  fi;
done;